if( Meteor.isServer ) {

  Meteor.startup(function() {
    Users = new Meteor.Collection('users');
  });

  // GET /Users
  // Retorna usuários salvos no banco
  Router.route('/api/v1/users',{where: 'server'})
    .get(function(){
      var response = {
        status: "succes",
        body: Users.find({}).fetch()
      };

      this.response.setHeader('Content-Type','application/json');
      this.response.end(JSON.stringify(response));

    })

  // POST /users
  // Atualiza um usuário no banco
    .post(function(){
      var response,
          body = this.request.body;

      if( body.username === undefined || body.password === undefined || body.name === undefined) {
        response = generateError("Dados não enviados.");
      } else {
        body.createdAt = new Date();
        var id = Users.insert(this.request.body);
        response = {
          status : "success",
          body : Users.findOne({_id: id})
        }
      }

      this.response.setHeader('Content-Type','application/json');
      this.response.end(JSON.stringify(response));

    })

  Router.route('/api/v1/users/:id', {where: 'server'})

  // GET /users/:id
  // Retorna um usuário específico
    .get(function(){
      var response;
      if(this.params.id !== undefined) {
        var data = Users.findOne({_id : this.params.id});
        if(!data) {
          response = generateError("Usuário não encontrado.");
        } else {
          response = {
            status : "success",
            body : data
          };
        }
      }

      this.response.setHeader('Content-Type','application/json');
      this.response.end(JSON.stringify(response));

    })

  // PUT /users/:id
  // Atualiza a data do usuário
    .put(function(){
      var response;
      if(this.params.id !== undefined) {
        var data = Users.findOne({_id : this.params.id});
        if(!data) {
          response = generateError("Usuário não encontrado.");
        }
        // Se existir algo no campo para mudar, então muda
        else if(this.request.body) {
          data.updatedAt = new Date();
          data.username = this.request.body.username;
          // A mudança do campo 'password' é opcional
          if(this.request.body.password) {
          data.password = this.request.body.password;
          }
          data.name = this.request.body.name;
        }

        Users.update({
          _id : data._id
        },{
          $set : data
        });

        response = {
          status : "success",
          body : data
        };

        this.response.setHeader('Content-Type','application/json');
        this.response.end(JSON.stringify(response));

      }
    })

    // DELETE /message/:id
    // Deleta um usuário específico
      .delete(function(){
        var response;
        if (this.params.id !== undefined) {
          var data = Users.findOne({_id : this.params.id});
          if(!data) {
            response = generateError("Contato não encontrado.");
          }

          Users.remove({_id : this.params.id});

          response = {
            status : "success",
            body : "Contato excluído com sucesso."
          };
        }

        this.response.setHeader('Content-Type','application/json');
        this.response.end(JSON.stringify(response));

      });

      // Functions

      function generateError(message){
        return {
          status: "fail",
          body: message
        };
      }
}

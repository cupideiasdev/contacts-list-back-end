if(Meteor.isServer) {

  Meteor.startup(function () {
    Contacts = new Meteor.Collection('contacts');
  });

  Router.route('/api/v1/contacts',{where: 'server'})
  // GET /contacts
  // Retorna contatos salvos no banco do usuário
    .get(function(){
      var response = {
        status: "success",
        body: Contacts.find({}).fetch()
      };

      this.response.setHeader('Content-Type','application/json');
      this.response.end(JSON.stringify(response));
    })

  // POST /contacts
  // Adiciona novo contato a coleção do usuário
    .post(function(){
      var response,
        body = this.request.body;

      if(body.phoneNumber === undefined) {
        response = generateError("Dados não enviados.");
      // Resposta de sucesso:
      } else {

        this.request.body.createdAt = new Date();

        var id = Contacts.insert(this.request.body);
        response = {
          "status" : "success",
          "body" : Contacts.findOne({_id: id})
        }
      }
      this.response.setHeader('Content-Type','application/json');
      this.response.end(JSON.stringify(response));
    });

  Router.route('/api/v1/contacts/:id', {where: 'server'})
  // GET /contacts/:id
  // Retorna um contato específico
    .get(function(){
      var response;
      if(this.params.id !== undefined) {
        var data = Contacts.findOne({_id : this.params.id});
        if(!data){
          response = generateError("Usuário não encontrado.");
        } else {
          response = {
            status: "success",
            body: data
          };
        }
      }
      this.response.setHeader('Content-Type','application/json');
      this.response.end(JSON.stringify(response));
    })

  // PUT /contacts/:id
  // Atualiza um registro específico
    .put(function(){
      var response;
      if(this.params.id !== undefined) {
        var data = Contacts.findOne({_id : this.params.id});

        if(!data)
          response = generateError("Contato não encontrado");

        data.updatedAt = new Date();
        data.phoneNumber = this.request.body.phoneNumber;

        Contacts.update({
          _id : data._id
        },{
          $set : data
        });

        response = {
          "status" : "success",
          "body" : data
        };

        this.response.setHeader('Content-Type','application/json');
        this.response.end(JSON.stringify(response));
      }
    })

  // DELETE /message/:id
  // Deleta um registro específico
    .delete(function(){
      var response;
      if(this.params.id !== undefined) {
        var data = Contacts.findOne({_id : this.params.id});

        if(!data)
          response = generateError("Contato não encontrado.");

        Contacts.remove({_id: this.params.id});

        response = {
          status: "success",
          body: "Contato excluído com sucesso."
        };
      }
      this.response.setHeader('Content-Type', 'application/json');
      this.response.end(JSON.stringify(response));
    });

    // Functions

    function generateError(message){
      return {
        status: "fail",
        body: message
      };
    }
}

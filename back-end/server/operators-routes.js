if(Meteor.isServer) {

  Meteor.startup(function () {
    Operators = new Meteor.Collection('operators');
  });

  Router.route('/api/v1/operators',{where: 'server'})
  // GET /Operators
  // Retorna operadoras salvas
    .get(function(){
      var response = {
        status: "success",
        body: Operators.find({}).fetch()
      };

      this.response.setHeader('Content-Type','application/json');
      this.response.end(JSON.stringify(response));
    })

  // POST /operators
  // Adiciona nova operadora
    .post(function(){
      var response,
        body = this.request.body;

      if(body.name === undefined || body.value === undefined) {
        response = generateError("Dados não enviados.");
      // Resposta de sucesso:
      } else {

        this.request.body.createdAt = new Date();

        var id = Operators.insert(this.request.body);
        response = {
          "status" : "success",
          "body" : Operators.findOne({_id: id})
        }
      }
      this.response.setHeader('Content-Type','application/json');
      this.response.end(JSON.stringify(response));
    });

  Router.route('/api/v1/operators/:id', {where: 'server'})
  // GET /operators/:id
  // Retorna uma operadora específica
    .get(function(){
      var response;
      if(this.params.id !== undefined) {
        var data = Operators.findOne({_id : this.params.id});
        if(!data){
          response = generateError("Usuário não encontrado.");
        } else {
          response = {
            status: "success",
            body: data
          };
        }
      }
      this.response.setHeader('Content-Type','application/json');
      this.response.end(JSON.stringify(response));
    })

  // PUT operators/:id
  // Atualiza um registro específico
    .put(function(){
      var response;
      if(this.params.id !== undefined) {
        var data = Operators.findOne({_id : this.params.id});

        if(!data)
          response = generateError("Contato não encontrado");

        data.updatedAt = new Date();
        if(this.request.body !== null) {
          data.name = this.request.body.name;
          data.value = this.request.body.value;
        }

        Operators.update({
          _id : data._id
        },{
          $set : data
        });

        response = {
          "status" : "success",
          "body" : data
        };

        this.response.setHeader('Content-Type','application/json');
        this.response.end(JSON.stringify(response));
      }
    })

  // DELETE /message/:id
  // Deleta um registro específico
    .delete(function(){
      var response;
      if(this.params.id !== undefined) {
        var data = Operators.findOne({_id : this.params.id});

        if(!data)
          response = generateError("Contato não encontrado.");

        Operators.remove({_id: this.params.id});

        response = {
          status: "success",
          body: "Contato excluído com sucesso."
        };
      }
      this.response.setHeader('Content-Type', 'application/json');
      this.response.end(JSON.stringify(response));
    });

    // Functions

    function generateError(message){
      return {
        status: "fail",
        body: message
      };
    }
}

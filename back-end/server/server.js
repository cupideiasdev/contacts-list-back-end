// if(Meteor.isServer) {
//
//   Meteor.startup(function () {
//     User = new Meteor.Collection('users');
//   });
//
//   // GET /user
//   // Retorna toda mensagem da coleção do MongoDB
//   Router.route('/api/v1/contacts',{where: 'server'})
//     .get(function(){
//       var response = User.find().fetch();
//       this.response.setHeader('Content-Type','application/json');
//       this.response.end(JSON.stringify(response));
//     })
//
//   // POST /message
//   // Adiciona nova mensagem na coleção do MongoDB
//     .post(function(){
//       var response;
//       if(this.request.body.userName === undefined || this.request.body.userPassword === undefined
//           || this.request.body.userDate === undefined || this.request.body.userNumber == undefined) {
//         response = {
//             "status" : "not success",
//             "message" : "invalid data"
//         };
//       // Resposta de sucesso:
//       } else {
//         User.insert({
//           UserName : this.request.body.userName,
//           UserPassword : this.request.body.userPassword,
//           UserDate : this.request.body.userDate = new Date(),
//           UserNumber : this.request.body.userNumber
//         });
//         response = {
//           "status" : "success",
//           "body" : "Objeto inserido no banco"
//         }
//       }
//       this.response.setHeader('Content-Type','application/json');
//       this.response.end(JSON.stringify(response));
//     });
//
//   Router.route('/api/v1/contacts/:id', {where: 'server'})
//   // GET /message/:id
//   // Retorna mensagem específica
//     .get(function(){
//       var response;
//       if(this.params.id !== undefined) {
//         var data = User.find({_id : this.params.id}).fetch();
//         if(data.length > 0) {
//           response = data
//         } else {
//           response = {
//             "status" : "not success",
//             "message" : "User not found."
//           }
//         }
//       }
//       this.response.setHeader('Content-Type','application/json');
//       this.response.end(JSON.stringify(response));
//     })
//
//   // PUT /message/:id
//   // Atualiza um registro específico
//     .put(function(){
//       var response;
//       if(this.params.id !== undefined) {
//         var data = User.find({_id : this.params.id}).fetch();
//         if(data.length > 0) {
//           if(User.update({_id : data[0]._id},{$set : {UserName : this.request.body.userName,
//               UserPassword : this.request.body.userPassword,UserDate : this.request.body.userDate,}}) === 1) {
//             response = {
//               "status" : "success",
//               "message" : "User information updated."
//             }
//           } else {
//             response = {
//               "status" : "not success",
//               "message" : "User not found."
//             }
//           }
//         }
//         this.response.setHeader('Content-Type','application/json');
//         this.response.end(JSON.stringify(response));
//       }
//     })
//
//   // DELETE /message/:id
//   // Deleta um registro específico
//     .delete(function(){
//       var response;
//       if(this.params.id !== undefined) {
//         var data = User.find({_id : this.params.id}).fetch();
//         if(data.length > 0) {
//           if(User.remove(data[0]._id) === 1) {
//             response = {
//               "status" : "success",
//               "message" : "User deleted."
//             }
//           } else {
//             response = {
//               "status" : "not success",
//               "message" : "User not deleted."
//             }
//           }
//         } else {
//           response = {
//             "status" : "not success",
//             "message" : "User not found."
//           }
//         }
//       }
//       this.response.setHeader('Content-Type', 'application/json');
//       this.response.end(JSON.stringify(response));
//     });
// }
